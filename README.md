- Expand file system and enable picamera.
    
        sudo raspi-config
        
- Update and upgrade packages.
    
        sudo apt-get update
        sudo apt-get upgrade

- Install dependencies.
    
        sudo apt-get install evince
        sudo apt-get install iceweasel
        sudo apt-get install python-picamera
        sudo apt-get install python-gst0.10 gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly
        
- Install Kivy on the RPi.
    
    Install Kivy.
        
        sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
           pkg-config libgl1-mesa-dev libgles2-mesa-dev \
           python-setuptools libgstreamer1.0-dev git-core \
           gstreamer1.0-plugins-{bad,base,good,ugly} \
           gstreamer1.0-{omx,alsa} python-dev cython
        
        sudo pip install git+https://github.com/kivy/kivy.git@master
        
        sudo apt-get install python-kivy-examples
        
    Test kivy with this:
        
        python -m kivy.examples.demo.showcase.main
    
    Config kivy for the touch screen.
    
    If you are using the official Raspberry Pi touch display, you need to configure Kivy to use it as an input source. 
    To do this, edit the file `~/.kivy/config.ini` and go to the `[input]` section. Add this:
        
        mouse = mouse
        mtdev_%(name)s = probesysfs,provider=mtdev
        hid_%(name)s = probesysfs,provider=hidinput
    
    Change the log level to `error`
    
- Run APP.
    Copy all source files to `/home/pi/solder`.
    At first, try to run `sudo python /home/pi/solder`.
    Touch will not work, but no matter. Let us fix it.
        
        sudo cp /home/pi/.kivy/config.ini /root/.kivy/config.ini

- Enable camera
        
    Open the `/etc/rc.local` and add this.
    
        sudo modprobe bcm2835-v4l2
            
    Open the `/boot/cmdline.txt` and add `bcm2708.w1_gpio_pin=18` at the end of line.
    
- Enable auto start(Optional)
    
        sudo nano /etc/profile
    
    Add the following line at the end of the file
        
        /usr/bin/python /home/pi/solder/watchdog.py
    
    Open the `raspi-config` and set `Boot Option` as `Desktop Autologin`.

- Rotate display
    
        sudo nano /boot/config.txt
        
    And add this at the end of the file.

        display_rotate=2
        
- Enable single-click
    
        sudo nano .config/libfm/libfm.conf
    Change 
        
        single_click=0
    to
        
        single_click=1
    And 
        
        sudo reboot
    
- Hide task bar (Optional).
    
    Edit the configuration file of the panel :

        sudo nano /home/pi/.config/lxpanel/LXDE-pi/panels/panel
    At the very top of the file, in the General section, you can modify these parameters

        autohide=0
        heightwhenhidden=2
    You can replace them by
        
        autohide=1
        heightwhenhidden=0
    if you want it to completely disappear
    
- Create desktop shortcut. 
    
        nano ~/Desktop/app.desktop
        
    And add this:
        
        [Desktop Entry]
        Name=App
        Comment=Soldering app
        Icon=/home/pi/solder/img/desktop_icon.png
        Exec=/usr/bin/python /home/pi/solder/watchdog.py
        Type=Application
        Encoding=UTF-8
        Terminal=true
        Categories=None;
    
    If I set `Terminal=false`, camera preview mode is closed after 5 sec.
    Not sure why this occurs.
    
- Make desktop icon larger.
    
        nano ~/.config/libfm/libfm.conf
    
    In the `[ui]` section, change the `pane_icon_size` to 48.
    