import logging
import os
import subprocess
import threading
import xml.etree.ElementTree
import time

import signal

log_level = 10
log_file_name = '/home/pi/solder/log.txt'
logging.basicConfig(level=log_level, filename=log_file_name,
                    format='%(asctime)s: %(message)s', datefmt='%m-%d-%Y %H:%M:%S')
conf_file_name = "/home/pi/solder/config.xml"


def check_process(proc_name):
    """
    Search the process and kill it.
    """
    pid_list = []
    cmd_result = os.popen("ps ax | grep " + proc_name + " | grep -v grep")

    for line in cmd_result:
        fields = line.split()
        pid_list.append(fields[0])
    logging.info(pid_list)
    if len(pid_list) > 0:
        return 'running'
    else:
        return 'closed'


def restart_preview():
    """
    Starting preview mode.
    This funcation is called with thread.
    Get position of the left top corner and zoom from the config file.
    """
    # self.popup.open()
    pos_x = get_param_from_xml("POS_X")
    pos_y = get_param_from_xml("POS_Y")
    height = get_param_from_xml("HEIGHT")
    width = get_param_from_xml("WIDTH")
    cmd = "raspistill -p " + pos_x + "," + pos_y + "," + width + "," + height + " -k"
    print cmd
    os.system(cmd)


def set_param_to_xml(tag_name, new_val):
    et = xml.etree.ElementTree.parse(conf_file_name)
    for child_of_root in et.getroot():
        if child_of_root.tag == tag_name:
            child_of_root.text = new_val
            et.write(conf_file_name)
            return True
    return False


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break

    return tmp


def kill_process(proc_name):
    """
    Search the process and kill it.
    """
    pid_list = []
    cmd_result = os.popen("ps ax | grep " + proc_name + " | grep -v grep")

    for line in cmd_result:
        fields = line.split()
        pid_list.append(fields[0])
    # if app is already running, the list's size is greater than 2
    if len(pid_list) > 0:
        for pid in pid_list:
            os.kill(int(pid), signal.SIGKILL)  # kill all process


if __name__ == "__main__":

    set_param_to_xml('STATUS', '')
    kill_process('/solder/main.py')

    print "Starting app..."

    while True:

        subprocess.Popen('/usr/bin/python /home/pi/solder/main.py', shell=True)
        print "Running main GUI"

        while True:
            try:
                cur_status = get_param_from_xml('STATUS')
            except xml.etree.ElementTree.ParseError as e:
                continue

            if cur_status == 'run_pdf':
                kill_process('/solder/main.py')
                pdf_file = get_param_from_xml('PDF_FILE')
                subprocess.Popen('/usr/bin/evince ' + pdf_file, shell=True)
                print "Running pdf viewer..."
                while check_process('evince') == 'running':
                    time.sleep(0.3)
                set_param_to_xml('STATUS', '')
                break
            elif cur_status == 'run_browser':
                kill_process('/solder/main.py')
                url = get_param_from_xml('HELP_URL')
                subprocess.Popen('/usr/bin/epiphany ' + url, shell=True)
                print "Running web browser, please be patient..."
                while check_process('epiphany') == 'running':
                    time.sleep(0.3)
                set_param_to_xml('STATUS', '')
                break
            elif cur_status == 'close_app':
                kill_process('/solder/main.py')
                set_param_to_xml('STATUS', '')
                exit(0)
            # elif cur_status == 'end_learn':
            #     kill_process('/solder/main.py')
            #     set_param_to_xml('STATUS', '')
            #     break
            # elif cur_status == 'preview':
            #     print "Preview mode..."
                # if check_process('raspistill') == 'closed':
                #     t = threading.Thread(target=restart_preview)
                #     t.daemon = True
                #     t.start()
            else:
                time.sleep(0.3)
