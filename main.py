import subprocess

import time

from kivy.uix.floatlayout import FloatLayout
from kivy.app import App
from kivy.uix.popup import Popup
from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty, ListProperty, NumericProperty, ObjectProperty, BooleanProperty
from kivy.clock import Clock
import os
from base import Base
import threading


class Picture(Scatter):
    """
    Scatter class containing image.
    """

    # consider this value weather updating picture with picamera should be performed or not.
    b_touched = BooleanProperty(False)

    # source of image file
    source = StringProperty(None)

    def on_touch_down(self, touch):
        super(Picture, self).on_touch_down(touch)
        self.b_touched = True

    def on_touch_up(self, touch):
        super(Picture, self).on_touch_up(touch)

        if main_app.mode == 'learn':
            # Move scatter widget if edge of it is in screen.
            if self.x > 0:
                self.x = 0

            if self.y > 0:
                self.y = 0

            offset = 800 - self.scale * self.width - self.x
            if offset > 0:
                self.x += offset

            offset = 480 - self.scale * self.height - self.y
            if offset > 0:
                self.y += offset

        self.b_touched = False


class ConfirmPopUp(Popup):
    pass


class ImageButton(FloatLayout):
    source = StringProperty(None)


class BlankLayout(FloatLayout):

    def on_touch_down(self, touch):
        super(BlankLayout, self).on_touch_down(touch)

        # walk throughout all widgets and decide which widget is touched
        t_type = None
        for _id in main_app.root.ids.keys():
            wid = main_app.root.ids[_id]
            if wid.collide_point(*touch.pos):
                t_type = _id
                break

        if main_app.mode in ['learn', 'preview']:
            main_app.touch_blank(touch)
        else:
            if t_type == 'microscope':
                main_app.mode = 'preview'
                t = threading.Thread(target=main_app.start_preview)
                t.daemon = True
                t.start()
            elif t_type == 'btn_doc':
                main_app.set_param_to_xml('STATUS', 'run_pdf')
                main_app.stop()
            elif t_type == 'web_browser':
                main_app.set_param_to_xml('STATUS', 'run_browser')
                main_app.stop()
            elif t_type == 'settings':
                main_app.set_param_to_xml('STATUS', 'learn')
                main_app.start_learn_mode()
            # elif t_type == 'update_firmware':
            #     print 'Updating firmware...'
            elif t_type == 'close_app':
                main_app.set_param_to_xml('STATUS', 'close_app')
            # elif t_type == 'blank':
                # when none of buttons are touched but blank area is touched.
                # main_app.touch_blank(touch)


class PicturesApp(App, Base):

    root = ObjectProperty(None)
    picture = ObjectProperty(None)
    camera = ObjectProperty(None)
    img_name = StringProperty('tmp.jpg')
    wid_img = ObjectProperty(None)
    mode = StringProperty('None')
    current_touch = None
    popup = ObjectProperty(None)

    def build(self):

        Base.__init__(self)
        self.mode = 'None'
        self.popup = ConfirmPopUp()
        self.set_param_to_xml('STATUS', '')

        # add scatter widget. Initial pos_y is -2000 to hide this widget at the starting time.
        self.picture = Picture(source=self.img_name, do_rotation=False, pos=[0, -2000], scale_min=1., scale_max=3.)
        self.root.add_widget(self.picture)
        self.wid_img = self.picture.children[0]

    def start_preview(self):
        """
        Starting preview mode.
        This function is called with thread.
        Get position of the left top corner and zoom from the config file.
        """
        # self.popup.open()
        pos_x = self.get_param_from_xml("POS_X")
        pos_y = self.get_param_from_xml("POS_Y")
        height = self.get_param_from_xml("HEIGHT")
        width = self.get_param_from_xml("WIDTH")
        cmd = "raspistill -p " + pos_x + "," + pos_y + "," + width + "," + height + " -k"
        os.system(cmd)

    def touch_blank(self, touch):
        # decide single tap or double tap
        if self.current_touch is not None:  # if already tapped
            # Calculate distance from the previous touch to ignore multi-touch
            if abs(touch.pos[0] - self.current_touch.pos[0]) + abs(touch.pos[1] - self.current_touch.pos[1]) < 20:
                Clock.unschedule(self.single_blank_touch)
                self.double_blank_touch(touch)
                self.current_touch = None
        # perform function after 0.5 sec. If another tap is occurred within 0.2 sec, it will be treated as double tap
        else:
            self.current_touch = touch
            Clock.schedule_once(self.single_blank_touch, 0.5)

    def single_blank_touch(self, *args):
        # if self.mode == 'preview':
        #     self.kill_process('raspistill')
        #     self.start_learn_mode()
        self.current_touch = None       # Important, must clear this value.

    def double_blank_touch(self, touch):
        if self.mode == 'learn':
            self.camera.close()
            Clock.unschedule(self.update_pic)
            self.update_zoom_pan()
            # hide scatter widget by subtracting 2000
            self.picture.pos = (self.picture.pos[0], self.picture.pos[1] - 2000)
            self.mode = 'None'
        elif self.mode == 'preview':
            self.mode = 'None'
            self.kill_process('raspistill')

    def start_learn_mode(self):
        """
        Start learn mode.
        """
        self.mode = 'learn'
        # Display scatter widget by resuming it position.
        import picamera
        self.camera = picamera.PiCamera()
        self.camera.resolution = (800, 480)
        self.update_pic()
        self.picture.pos = (self.picture.pos[0], self.picture.pos[1] + 2000)
        Clock.schedule_interval(self.update_pic, float(self.get_param_from_xml('REFRESH_TIME')))

    def update_pic(self, *args):
        """
        This function is called in the learn mode only.
        """
        if not self.debug:
            s_time = time.time()
            if not self.picture.b_touched:  # update when user does not touch image.
                self.camera.capture_sequence([self.img_name], use_video_port=True)      # takes about 0.12 sec
                self.wid_img.reload()  # takes about 0.1 sec

    def update_zoom_pan(self):
        """
        Calculate pan and zoom and update parameters in the config file.
        """
        # calculate pan and zoom
        x = int(self.picture.pos[0])
        y = int(self.picture.pos[1])
        scale = float(self.picture.scale)

        print x, ": ", y, "; ", scale

        b = int(480 - y - 480 * scale)

        if scale > 2.0:  # Not sure why this is needed, anyhow it works... lol
            b += 240

        width = int(800.0 * scale)
        height = int(480.0 * scale)

        if b > 0:       # If y-position is greater than 0, concentrate its height
            height += b * 2
            b = 0

        if width == 0 or height == 0:
            return False

        self.set_param_to_xml('POS_X', str(x))
        self.set_param_to_xml('POS_Y', str(b))
        self.set_param_to_xml('WIDTH', str(width))
        self.set_param_to_xml('HEIGHT', str(height))


main_app = PicturesApp()

if __name__ == '__main__':

    main_app.run()
