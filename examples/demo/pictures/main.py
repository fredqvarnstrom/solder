'''
Basic Picture Viewer
====================

This simple image browser demonstrates the scatter widget. You should
see three framed photographs on a background. You can click and drag
the photos around, or multi-touch to drop a red dot to scale and rotate the
photos.

The photos are loaded from the local images directory, while the background
picture is from the data shipped with kivy in kivy/data/images/background.jpg.
The file pictures.kv describes the interface and the file shadow32.png is
the border to make the images look like framed photographs. Finally,
the file android.txt is used to package the application for use with the
Kivy Launcher Android application.

For Android devices, you can copy/paste this directory into
/sdcard/kivy/pictures on your Android device.

The images in the image directory are from the Internet Archive,
`https://archive.org/details/PublicDomainImages`, and are in the public
domain.

'''

import kivy
kivy.require('1.0.6')

from glob import glob
from random import randint
from os.path import join, dirname
from kivy.app import App
from kivy.logger import Logger
from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty, ListProperty, NumericProperty, ObjectProperty
from kivy.clock import Clock


class Picture(Scatter):
    '''Picture is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''

    source = StringProperty(None)


class PicturesApp(App):

    file_list = ListProperty([])
    cnt = NumericProperty(0)
    picture = ObjectProperty(None)

    def build(self):

        # the root is created in main.kv
        root = self.root

        # get any files into images directory
        curdir = dirname(__file__)
        self.file_list = glob(join(curdir, 'images', '*'))

        Clock.schedule_interval(self.change_pic, 0.5)

        # load the image
        self.picture = Picture(id='pic', source=self.file_list[0], rotation=randint(-30, 30))
        # add to the main field
        root.add_widget(self.picture)

    def on_pause(self):
        return True

    def change_pic(self, *args):
        if self.cnt < len(self.file_list) - 1:
            self.cnt += 1
        else:
            self.cnt = 0
        print self.cnt, " | ", self.picture.source
        self.picture.source = self.file_list[self.cnt]

if __name__ == '__main__':
    PicturesApp().run()
