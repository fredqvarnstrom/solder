import os
import xml.etree.ElementTree

import signal


class Base:

    _version = 1.0
    conf_file_name = ''
    debug = False

    @property
    def version(self):
        """
        get
        :return:
        """
        return self._version

    @version.setter
    def version(self, value):
        self._version = value

    def __init__(self):
        if self.debug:
            self.conf_file_name = 'config.xml'
        else:
            self.conf_file_name = "/home/pi/solder/config.xml"

    def set_param_to_xml(self, tag_name, new_val):
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break

        return tmp

    def kill_process(self, proc_name):
        """
        Search the process and kill it.
        """
        pid_list = []
        cmd_result = os.popen("ps ax | grep " + proc_name + " | grep -v grep")

        for line in cmd_result:
            fields = line.split()
            pid_list.append(fields[0])
        # print "PID: ", pid_list
        # if app is already running, the list's size is greater than 2
        if len(pid_list) > 0:
            for pid in pid_list:
                os.kill(int(pid), signal.SIGKILL)  # kill all process

if __name__ == '__main__':
    inst = Base()
    print(inst)
