import time
import picamera


with picamera.PiCamera() as camera:
    camera.resolution = (800, 480)
    start = time.time()
    camera.capture_sequence(['1.jpg'], use_video_port=True)
    finish = time.time()

print 'Elapsed: ', finish - start

import os

import signal


def kill_process(proc_name):
    """
    Search the process and kill it.
    """
    pid_list = []
    cmd_result = os.popen("ps ax | grep " + proc_name + " | grep -v grep")

    for line in cmd_result:
        fields = line.split()
        pid_list.append(fields[0])
    print "PID: ", pid_list
    # if app is already running, the list's size is greater than 2
    if len(pid_list) > 0:
        for pid in pid_list:
            os.kill(int(pid), signal.SIGKILL)  # kill all process

